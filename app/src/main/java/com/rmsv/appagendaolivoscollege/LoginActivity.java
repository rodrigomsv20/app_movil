package com.rmsv.appagendaolivoscollege;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.rmsv.appagendaolivoscollege.model.Alumno;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnIngresar;
    private TextInputEditText etUsuario, etContrasenia;
    private SharedPreferences preferences;
    private String urlAutentificacion = "";
    public Alumno alumno = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnIngresar = findViewById(R.id.btnIngresar);
        etUsuario = findViewById(R.id.etUsuario);
        etContrasenia = findViewById(R.id.etContrasenia);


        btnIngresar.setOnClickListener(this);

        preferences = getSharedPreferences("appADOC",MODE_PRIVATE);
        if(preferences.contains("usuarioAlumno")){
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            finish();
        }
    }

    @Override
    public void onClick(View view) {
        String usuario = etUsuario.getText().toString();
        String contrasenia = etContrasenia.getText().toString();
        if(usuario.trim().equals("") || contrasenia.trim().equals("")){
            Log.i("usuario",usuario);
            Log.i("contrasenia",contrasenia);
            Toast.makeText(LoginActivity.this, "¡Debes ingresar usuario y contraseña!", Toast.LENGTH_LONG).show();
        }else{
            urlAutentificacion = "";
            urlAutentificacion = "http://192.168.0.16:8090/awc/autentificar/Alumno";
            JSONObject postData = new JSONObject();
            try {
                postData.put("usuario_alumno", usuario);
                postData.put("clave", contrasenia);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            autentificacionWS(urlAutentificacion,postData);
        }

    }

    private void autentificacionWS(String urlAutentificacion,JSONObject postData){
        Log.i("entro aca",urlAutentificacion);
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                urlAutentificacion,
                postData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            alumno = new Alumno(
                                    response.getInt("alumno_id"),
                                    response.getInt("dni_alumno"),
                                    response.getString("nombres"),
                                    response.getString("apellidos"),
                                    response.getString("fecha_nacimiento"),
                                    response.getString("direccion"),
                                    response.getString("fecha_registro"),
                                    response.getString("usuario_alumno"),
                                    response.getString("estado")
                            );
                            Log.i("Alumno",alumno.toString());
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("usuarioAlumno",alumno.getUsuario_alumno());
                            editor.putString("nombreAlumno",alumno.getNombres());
                            editor.putString("apellidoAlumno",alumno.getApellidos());
                            editor.putInt("idAlumno",alumno.getAlumno_id());
                            editor.apply();
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        }catch (JSONException ex){
                            Log.i("Error","");
                            ex.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String json = null;
                        NetworkResponse response = error.networkResponse;
                        Log.i("response error",Integer.toString(response.statusCode));
                        if(response != null && response.data != null){
                            switch (response.statusCode){
                                case 404:
                                    json = new String(response.data);
                                    json = extraerMensaje(json, "mensaje");
                                    if(json != null)
                                        mostrarMensaje(json);
                                    break;
                            }
                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    public static String extraerMensaje(String json, String key){
        String mensaje = null;
        try{
            JSONObject obj = new JSONObject(json);
            mensaje = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }
        return mensaje;

    }

    public void mostrarMensaje(String toastString){
        Toast.makeText(LoginActivity.this, toastString, Toast.LENGTH_LONG).show();
    }
}