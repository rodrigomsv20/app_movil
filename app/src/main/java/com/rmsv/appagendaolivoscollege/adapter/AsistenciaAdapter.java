package com.rmsv.appagendaolivoscollege.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rmsv.appagendaolivoscollege.R;
import com.rmsv.appagendaolivoscollege.model.Asistencia;

import java.util.ArrayList;

public class AsistenciaAdapter extends RecyclerView.Adapter<AsistenciaAdapter.ViewHolder> {

    private ArrayList<Asistencia> listaAsistencia;
    private Context context;

    public AsistenciaAdapter(Context context) {
        this.context = context;
        listaAsistencia = new ArrayList<>();
    }

    @NonNull
    @Override
    public AsistenciaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_asistencia,parent,false);
        context = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AsistenciaAdapter.ViewHolder holder, int position) {
        final Asistencia objAsistencia = listaAsistencia.get(position);
        holder.tvCurso.setText(objAsistencia.getNombre());
        holder.tvAsistencia.setText(objAsistencia.getAsistio());
    }

    public void agregarAsistencia(ArrayList<Asistencia> listAsistencias){
        listaAsistencia.clear();
        listaAsistencia.addAll(listAsistencias);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listaAsistencia.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCurso,tvAsistencia;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCurso = itemView.findViewById(R.id.tvCurso);
            tvAsistencia = itemView.findViewById(R.id.tvAsistencia);
        }
    }
}
