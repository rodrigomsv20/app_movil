package com.rmsv.appagendaolivoscollege.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rmsv.appagendaolivoscollege.R;
import com.rmsv.appagendaolivoscollege.model.Incidencia;

import java.util.ArrayList;

public class IncidenciaAdapter extends RecyclerView.Adapter<IncidenciaAdapter.ViewHolder> {

    private ArrayList<Incidencia> listaIncidencia;
    private Context context;

    public IncidenciaAdapter(Context context){
        this.context = context;
        listaIncidencia = new ArrayList<>();
    }

    @NonNull
    @Override
    public IncidenciaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_incidencia,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IncidenciaAdapter.ViewHolder holder, int position) {
        final Incidencia incidencia = listaIncidencia.get(position);
        holder.tvCurso.setText(incidencia.getNombre());
        holder.tvIncidencia.setText("Incidencia:" + incidencia.getIncidencia());
    }

    @Override
    public int getItemCount() {
        return listaIncidencia.size();
    }

    public void agregarIncidencias(ArrayList<Incidencia> listaIncidencias){
        listaIncidencia.clear();
        listaIncidencia.addAll(listaIncidencias);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvCurso, tvIncidencia;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCurso = itemView.findViewById(R.id.tvCurso);
            tvIncidencia = itemView.findViewById(R.id.tvIncidencia);
        }
    }

}
