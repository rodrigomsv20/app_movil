package com.rmsv.appagendaolivoscollege.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rmsv.appagendaolivoscollege.R;
import com.rmsv.appagendaolivoscollege.model.Calificacion;

import java.util.ArrayList;

public class CalificacionAdapter extends RecyclerView.Adapter<CalificacionAdapter.ViewHolder> {

    private ArrayList<Calificacion> listaCalificacion;
    private Context context;

    public CalificacionAdapter(Context context) {
        this.context = context;
        listaCalificacion = new ArrayList<>();
    }

    @NonNull
    @Override
    public CalificacionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calificacion,parent,false);
        context = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CalificacionAdapter.ViewHolder holder, int position) {
        final Calificacion objCalificacion = listaCalificacion.get(position);
        holder.tvCurso.setText(objCalificacion.getNombre());
        Integer promPracCalif = 0;
        promPracCalif = (objCalificacion.getPractica_calificada_1() + objCalificacion.getPractica_calificada_2() + objCalificacion.getPractica_calificada_3()) /3;
        holder.tvPromPC.setText(String.valueOf(promPracCalif));
        holder.tvExFinal.setText(String.valueOf(objCalificacion.getExamen_final()));
        holder.tvPromFinal.setText(String.valueOf(objCalificacion.getPromedio()));

    }

    public void agregarCalificacion(ArrayList<Calificacion> listCalificaciones){
        listaCalificacion.clear();
        listaCalificacion.addAll(listCalificaciones);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listaCalificacion.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCurso,tvPromPC,tvExFinal,tvPromFinal;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCurso = itemView.findViewById(R.id.tvCurso);
            tvPromPC = itemView.findViewById(R.id.tvPromPC);
            tvExFinal = itemView.findViewById(R.id.tvExFinal);
            tvPromFinal = itemView.findViewById(R.id.tvPromFinal);
        }
    }
}
