package com.rmsv.appagendaolivoscollege.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rmsv.appagendaolivoscollege.R;
import com.rmsv.appagendaolivoscollege.model.Tarea;

import java.util.ArrayList;

public class TareaAdapter extends RecyclerView.Adapter<TareaAdapter.ViewHolder> {

    private ArrayList<Tarea> listaTarea;
    private Context context;

    public TareaAdapter(Context context) {
        this.context = context;
        listaTarea = new ArrayList<>();
    }

    @NonNull
    @Override
    public TareaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tarea,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TareaAdapter.ViewHolder holder, int position) {
        final Tarea objTarea = listaTarea.get(position);
        holder.tvCurso.setText(objTarea.getNombre());
        holder.tvNota.setText("Tarea:" + objTarea.getTarea());
    }

    @Override
    public int getItemCount() {
        return listaTarea.size();
    }

    public void agregarTareas(ArrayList<Tarea> listTareas){
        listaTarea.clear();
        listaTarea.addAll(listTareas);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvCurso, tvNota;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCurso = itemView.findViewById(R.id.tvCurso);
            tvNota = itemView.findViewById(R.id.tvTarea);
        }
    }
}
