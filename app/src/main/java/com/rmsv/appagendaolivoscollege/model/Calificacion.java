package com.rmsv.appagendaolivoscollege.model;

public class Calificacion {
    private String nombre;
    private int practica_calificada_1;
    private int practica_calificada_2;
    private int practica_calificada_3;
    private int examen_final;
    private int promedio;

    public Calificacion(String nombre, int practica_calificada_1, int practica_calificada_2, int practica_calificada_3, int examen_final, int promedio) {
        this.nombre = nombre;
        this.practica_calificada_1 = practica_calificada_1;
        this.practica_calificada_2 = practica_calificada_2;
        this.practica_calificada_3 = practica_calificada_3;
        this.examen_final = examen_final;
        this.promedio = promedio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPractica_calificada_1() {
        return practica_calificada_1;
    }

    public void setPractica_calificada_1(int practica_calificada_1) {
        this.practica_calificada_1 = practica_calificada_1;
    }

    public int getPractica_calificada_2() {
        return practica_calificada_2;
    }

    public void setPractica_calificada_2(int practica_calificada_2) {
        this.practica_calificada_2 = practica_calificada_2;
    }

    public int getPractica_calificada_3() {
        return practica_calificada_3;
    }

    public void setPractica_calificada_3(int practica_calificada_3) {
        this.practica_calificada_3 = practica_calificada_3;
    }

    public int getExamen_final() {
        return examen_final;
    }

    public void setExamen_final(int examen_final) {
        this.examen_final = examen_final;
    }

    public int getPromedio() {
        return promedio;
    }

    public void setPromedio(int promedio) {
        this.promedio = promedio;
    }
}
