package com.rmsv.appagendaolivoscollege.model;

public class Incidencia {
    private String nombre;
    private String incidencia;

    public Incidencia(String nombre, String incidencia) {
        this.nombre = nombre;
        this.incidencia = incidencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIncidencia() {
        return incidencia;
    }

    public void setIncidencia(String incidencia) {
        this.incidencia = incidencia;
    }
}
