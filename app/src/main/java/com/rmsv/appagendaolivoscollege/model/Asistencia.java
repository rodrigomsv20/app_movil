package com.rmsv.appagendaolivoscollege.model;

public class Asistencia {
    public String nombre;
    public String asistio;

    public Asistencia(String nombre, String asistio) {
        this.nombre = nombre;
        this.asistio = asistio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAsistio() {
        return asistio;
    }

    public void setAsistio(String asistio) {
        this.asistio = asistio;
    }
}
