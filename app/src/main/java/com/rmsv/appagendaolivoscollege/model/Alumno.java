package com.rmsv.appagendaolivoscollege.model;

public class Alumno {
    private Integer alumno_id;
    private Integer dni_alumno;
    private String nombres;
    private String apellidos;
    private String fecha_nacimiento;
    private String direccion;
    private String fecha_registro;
    private String usuario_alumno;
    private String estado;

    public Alumno(Integer alumno_id, Integer dni_alumno, String nombres, String apellidos, String fecha_nacimiento, String direccion, String fecha_registro, String usuario_alumno, String estado) {
        this.alumno_id = alumno_id;
        this.dni_alumno = dni_alumno;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.fecha_nacimiento = fecha_nacimiento;
        this.direccion = direccion;
        this.fecha_registro = fecha_registro;
        this.usuario_alumno = usuario_alumno;
        this.estado = estado;
    }

    public Integer getAlumno_id() {
        return alumno_id;
    }

    public void setAlumno_id(Integer alumno_id) {
        this.alumno_id = alumno_id;
    }

    public Integer getDni_alumno() {
        return dni_alumno;
    }

    public void setDni_alumno(Integer dni_alumno) {
        this.dni_alumno = dni_alumno;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(String fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public String getUsuario_alumno() {
        return usuario_alumno;
    }

    public void setUsuario_alumno(String usuario_alumno) {
        this.usuario_alumno = usuario_alumno;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Alumno{" +
                "alumno_id=" + alumno_id +
                ", dni_alumno=" + dni_alumno +
                ", nombres='" + nombres + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", fecha_nacimiento='" + fecha_nacimiento + '\'' +
                ", direccion='" + direccion + '\'' +
                ", fecha_registro='" + fecha_registro + '\'' +
                ", usuario_alumno='" + usuario_alumno + '\'' +
                ", estado='" + estado + '\'' +
                '}';
    }
}
