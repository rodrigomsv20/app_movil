package com.rmsv.appagendaolivoscollege;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final Intent intentLogin = new Intent(this,LoginActivity.class);
        Thread timer = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(4000);
                }catch (InterruptedException ex){
                    ex.printStackTrace();
                }finally {
                    startActivity(intentLogin);
                    finish();
                }
            }
        };
        timer.start();
    }
}