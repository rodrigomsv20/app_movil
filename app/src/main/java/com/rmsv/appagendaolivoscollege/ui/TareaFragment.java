package com.rmsv.appagendaolivoscollege.ui;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.rmsv.appagendaolivoscollege.R;
import com.rmsv.appagendaolivoscollege.adapter.TareaAdapter;
import com.rmsv.appagendaolivoscollege.model.Tarea;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

public class TareaFragment extends Fragment {

    private RecyclerView rvTareas;
    private TareaAdapter adapter;

    private EditText etFechaTarea;
    private Button btnEditarFecha;
    private String urlTareas = "", fecha ="";
    private ArrayList<Tarea> listaTarea;
    private Integer idAlumno = 0;

    private int mDate, mMonth, mYear;

    public TareaFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tarea, container, false);
        rvTareas = view.findViewById(R.id.rvTareas);
        adapter = new TareaAdapter(getContext());
        rvTareas.setAdapter(adapter);
        rvTareas.setLayoutManager(new GridLayoutManager(getContext(),1));

        SharedPreferences preferences = this.getActivity().getSharedPreferences("appADOC",MODE_PRIVATE);
        idAlumno =  preferences.getInt("idAlumno",0);
        etFechaTarea = view.findViewById(R.id.etFechaTarea);
        btnEditarFecha = view.findViewById(R.id.btnEditarFechaAsis);
        etFechaTarea.setText(getFechaActual());

        btnEditarFecha.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mostrarCalendario();
                    }
                });

        llamadaServicio(getFechaActual());
        return view;
    }

    public static String getFechaActual() {
        Date actual = new Date();
        return formatoFecha(actual);
    }

    public static String formatoFecha(Date fecha){
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
        return formateador.format(fecha);
    }


    public void mostrarCalendario(){
        final Calendar cal = Calendar.getInstance();
        mDate = cal.get(Calendar.DATE);
        mMonth = cal.get(Calendar.MONTH);
        mYear = cal.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), android.R.style.Theme_DeviceDefault_Dialog
                , new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int date) {
                fecha = year + "-" + (month+1) + "-" + date;
                etFechaTarea.setText(date+"-"+(month+1)+"-"+year);
                llamadaServicio(fecha);
            }
        },mYear,mMonth,mDate);
        datePickerDialog.show();
    }

    public void obtenerTareasWS(String urlTareas){
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        listaTarea = new ArrayList<Tarea>();
        JsonArrayRequest jsonArray = new JsonArrayRequest(
                Request.Method.GET,
                urlTareas,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {
                        try{
                            if(jsonArray.length() > 0){
                                for(int i = 0; i < jsonArray.length(); i++){
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    listaTarea.add(new Tarea(
                                            jsonObject.getString("nombre"),
                                            jsonObject.getString("descripcion")
                                    ));
                                }
                                adapter.agregarTareas(listaTarea);
                            }
                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String json = null;
                        NetworkResponse response = error.networkResponse;
                        if(response != null && response.data != null){
                            switch (response.statusCode){
                                case 404:
                                    listaTarea.clear();
                                    adapter.agregarTareas(listaTarea);
                                    json = new String(response.data);
                                    json = extraerMensaje(json, "mensaje");
                                    if(json != null)
                                        mostrarMensaje(json);
                                    break;
                            }
                        }
                    }
                }
        );
        requestQueue.add(jsonArray);
    }



    public void llamadaServicio(String fecha){
        urlTareas = "";
        urlTareas = "http://192.168.0.16:8090/awc/tarea/listaTareasGradoFecha/" + idAlumno;
        urlTareas += "/"+fecha;
        Log.i("Get Servicio Tareas",urlTareas);
        obtenerTareasWS(urlTareas);
    }


    public String extraerMensaje(String json, String key){
        String mensaje = null;
        try{
            JSONObject obj = new JSONObject(json);
            mensaje = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }
        return mensaje;

    }

    public void mostrarMensaje(String toastString){
        Toast toast = Toast.makeText(getContext(), toastString, Toast.LENGTH_LONG);
        View view = toast.getView();
        view.getBackground().setColorFilter(Color.parseColor("#605C3C"), PorterDuff.Mode.SRC_IN);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(Color.WHITE);
        toast.show();
    }

}