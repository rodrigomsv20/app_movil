package com.rmsv.appagendaolivoscollege.ui;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.rmsv.appagendaolivoscollege.R;
import com.rmsv.appagendaolivoscollege.adapter.IncidenciaAdapter;
import com.rmsv.appagendaolivoscollege.model.Incidencia;
import com.rmsv.appagendaolivoscollege.model.Tarea;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class IncidenciaFragment extends Fragment {

    private RecyclerView rvIncidencias;
    private IncidenciaAdapter adapter;

    private EditText etFechaIncidencia;
    private Button btnEditarFecha;
    private String urlIncidencias = "", fecha ="";
    private ArrayList<Incidencia> listaIncidencia;
    private Integer idAlumno = 0;
    private int mDate, mMonth, mYear;

    public IncidenciaFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_incidencia, container, false);

        rvIncidencias = view.findViewById(R.id.rvIncidencias);
        adapter = new IncidenciaAdapter(getContext());
        rvIncidencias.setAdapter(adapter);
        rvIncidencias.setLayoutManager(new GridLayoutManager(getContext(),1));

        SharedPreferences preferences = this.getActivity().getSharedPreferences("appADOC", Context.MODE_PRIVATE);
        idAlumno =  preferences.getInt("idAlumno",0);
        etFechaIncidencia = view.findViewById(R.id.etFechaIncidencia);
        btnEditarFecha = view.findViewById(R.id.btnEditarFechaInc);
        etFechaIncidencia.setText(getFechaActual());

        btnEditarFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarCalendario();
            }
        });
        llamadaServicio(getFechaActual());
        return view;
    }

    public static String getFechaActual() {
        Date actual = new Date();
        return formatoFecha(actual);
    }

    public static String formatoFecha(Date fecha){
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
        return formateador.format(fecha);
    }

    public void mostrarCalendario(){
        final Calendar cal = Calendar.getInstance();
        mDate = cal.get(Calendar.DATE);
        mMonth = cal.get(Calendar.MONTH);
        mYear = cal.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), android.R.style.Theme_DeviceDefault_Dialog
                , new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int date) {
                fecha = year + "-" + (month+1) + "-" + date;
                etFechaIncidencia.setText(date+"-"+(month+1)+"-"+year);
                llamadaServicio(fecha);
            }
        },mYear,mMonth,mDate);
        datePickerDialog.show();
    }

    public void llamadaServicio(String fecha){
        urlIncidencias = "";
        urlIncidencias = "http://192.168.0.16:8090/awc/incidencia/listaIncsAlumnoFecha/" + idAlumno + "/"+ fecha;
        Log.i("Get Servicio Tareas",urlIncidencias);
        obtenerIncidenciasWS(urlIncidencias);
    }

    public void obtenerIncidenciasWS(String urlIncidencias){
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        listaIncidencia = new ArrayList<Incidencia>();
        JsonArrayRequest jsonArray = new JsonArrayRequest(
                Request.Method.GET,
                urlIncidencias,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {
                        try{
                            if(jsonArray.length() > 0){
                                for(int i = 0; i < jsonArray.length(); i++){
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    listaIncidencia.add(new Incidencia(
                                            jsonObject.getString("nombre"),
                                            jsonObject.getString("descripcion")
                                    ));
                                }
                                adapter.agregarIncidencias(listaIncidencia);
                            }
                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String json = null;
                        NetworkResponse response = error.networkResponse;
                        if(response != null && response.data != null){
                            switch (response.statusCode){
                                case 404:
                                    listaIncidencia.clear();
                                    adapter.agregarIncidencias(listaIncidencia);
                                    json = new String(response.data);
                                    json = extraerMensaje(json, "mensaje");
                                    if(json != null)
                                        mostrarMensaje(json);
                                    break;
                            }
                        }
                    }
                }
        );
        requestQueue.add(jsonArray);
    }

    public String extraerMensaje(String json, String key){
        String mensaje = null;
        try{
            JSONObject obj = new JSONObject(json);
            mensaje = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }
        return mensaje;

    }

    public void mostrarMensaje(String toastString){
        Toast toast = Toast.makeText(getContext(), toastString, Toast.LENGTH_LONG);
        View view = toast.getView();
        view.getBackground().setColorFilter(Color.parseColor("#605C3C"), PorterDuff.Mode.SRC_IN);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(Color.WHITE);
        toast.show();
    }

}