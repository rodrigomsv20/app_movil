package com.rmsv.appagendaolivoscollege.ui;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.rmsv.appagendaolivoscollege.R;
import com.rmsv.appagendaolivoscollege.adapter.CalificacionAdapter;
import com.rmsv.appagendaolivoscollege.model.Calificacion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class CalificacionFragment extends Fragment {

    //Variables de fragmento
    private Spinner spTrimestre;
    private Button btnBuscarTrimestre;
    private TextView tvCursoFrag,tvPromPCFrag,tvExFinalFrag,tvPromFinalFrag;
    private RecyclerView rvCalificaciones;
    private CalificacionAdapter adapter;
    private ArrayList<Calificacion> listaCalificacion;

    //Variables para Servicio
    private String urlCalificaciones= "";

    //Varibale para cookie
    private Integer idAlumno = 0;

    //Variables extras
    private String nroTrimestre;

    public CalificacionFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_calificacion, container, false);

        //Asignar valores del fragmento a variables locales
        spTrimestre = view.findViewById(R.id.spTrimestre);
        btnBuscarTrimestre = view.findViewById(R.id.btnBuscarTrimestre);
        rvCalificaciones = view.findViewById(R.id.rvCalificaciones);
        tvCursoFrag = view.findViewById(R.id.tvCursoFrag);
        tvPromPCFrag = view.findViewById(R.id.tvPromPCFrag);
        tvExFinalFrag = view.findViewById(R.id.tvExFinalFrag);
        tvPromFinalFrag = view.findViewById(R.id.tvPromFinalFrag);

        //Agregar texto a spinner
        String[] trimestres ={"Primer Trimestre","Segundo Trimestre","Tercer Trimestre"};
        ArrayAdapter<String> adapterSp = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,trimestres);
        spTrimestre.setAdapter(adapterSp);

        //Inicializar variables para adaptador
        adapter = new CalificacionAdapter(getContext());
        rvCalificaciones.setAdapter(adapter);
        rvCalificaciones.setLayoutManager(new GridLayoutManager(getContext(),1));

        //Asignar el id del alumno desde la cookie
        SharedPreferences preferences = this.getActivity().getSharedPreferences("appADOC",MODE_PRIVATE);
        idAlumno =  preferences.getInt("idAlumno",0);

        //Llamar a servicio para cuando apenas ingresa
        llamadaServicioCalif("1");

        //Acción al hacer clic en boton
        btnBuscarTrimestre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nroTrimestre = spTrimestre.getSelectedItem().toString();
                Log.i("nroTrimestre",nroTrimestre);
                if(nroTrimestre.equals("Primer Trimestre")){
                    llamadaServicioCalif("1");
                }else if(nroTrimestre.equals("Segundo Trimestre")){
                    llamadaServicioCalif("2");
                }else{
                    llamadaServicioCalif("3");
                }
            }
        });

        return view;
    }

    public void llamadaServicioCalif(String nroTrimestre){
        urlCalificaciones = "";
        urlCalificaciones = "http://192.168.0.16:8090/awc/calificacion/listaCalifAlumnoPerEscolar/" + idAlumno;
        urlCalificaciones += "/" + nroTrimestre;
        obtenerCalificacionesWS(urlCalificaciones);
    }

    private void obtenerCalificacionesWS(String urlCalificaciones) {
        Log.i("urlCalificaciones",urlCalificaciones);
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        listaCalificacion = new ArrayList<Calificacion>();
        JsonArrayRequest jsonArray = new JsonArrayRequest(
                Request.Method.GET,
                urlCalificaciones,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {
                        try{
                            if(jsonArray.length() > 0){
                                for(int i = 0; i < jsonArray.length(); i++){
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    listaCalificacion.add(new Calificacion(
                                            jsonObject.getString("nombre"),
                                            jsonObject.getInt("practica_calificada_1"),
                                            jsonObject.getInt("practica_calificada_2"),
                                            jsonObject.getInt("practica_calificada_3"),
                                            jsonObject.getInt("examen_final"),
                                            jsonObject.getInt("promedio")
                                    ));
                                }
                                adapter.agregarCalificacion(listaCalificacion);
                                tvCursoFrag.setVisibility(View.VISIBLE);
                                tvPromPCFrag.setVisibility(View.VISIBLE);
                                tvExFinalFrag.setVisibility(View.VISIBLE);
                                tvPromFinalFrag.setVisibility(View.VISIBLE);
                            }
                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String json = null;
                        NetworkResponse response = error.networkResponse;
                        if(response != null && response.data != null){
                            switch (response.statusCode){
                                case 404:
                                    listaCalificacion.clear();
                                    adapter.agregarCalificacion(listaCalificacion);
                                    json = new String(response.data);
                                    json = extraerMensaje(json, "mensaje");
                                    if(json != null)
                                        mostrarMensaje(json);
                                    break;
                            }
                        }
                    }
                }
        );
        requestQueue.add(jsonArray);
    }

    public String extraerMensaje(String json, String key){
        String mensaje = null;
        try{
            JSONObject obj = new JSONObject(json);
            mensaje = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }
        return mensaje;

    }

    public void mostrarMensaje(String toastString){
        Toast toast = Toast.makeText(getContext(), toastString, Toast.LENGTH_LONG);
        View view = toast.getView();
        view.getBackground().setColorFilter(Color.parseColor("#605C3C"), PorterDuff.Mode.SRC_IN);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(Color.WHITE);
        toast.show();
        tvCursoFrag.setVisibility(View.GONE);
        tvPromPCFrag.setVisibility(View.GONE);
        tvExFinalFrag.setVisibility(View.GONE);
        tvPromFinalFrag.setVisibility(View.GONE);
    }
}