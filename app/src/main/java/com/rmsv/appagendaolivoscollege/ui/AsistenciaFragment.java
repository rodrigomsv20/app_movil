package com.rmsv.appagendaolivoscollege.ui;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.rmsv.appagendaolivoscollege.R;
import com.rmsv.appagendaolivoscollege.adapter.AsistenciaAdapter;
import com.rmsv.appagendaolivoscollege.model.Asistencia;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

public class AsistenciaFragment extends Fragment {

    //Variables de fragmento
    private EditText etFechaAsistencia;
    private Button btnEditarFechaAsis;
    private TextView tvCursoFrag,tvAsistioFrag;
    private RecyclerView rvAsistencias;
    private AsistenciaAdapter adapter;
    private ArrayList<Asistencia> listaAsistencia;

    //Variables para Servicio
    private String urlAsistencias= "", fecha ="";

    //Variables extras
    private int mDate, mMonth, mYear;

    //Varibale para cookie
    private Integer idAlumno = 0;

    public AsistenciaFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_asistencia, container, false);

        //Asignar valores del fragmento a variables locales
        etFechaAsistencia = view.findViewById(R.id.etFechaAsistencia);
        btnEditarFechaAsis = view.findViewById(R.id.btnEditarFechaAsis);
        rvAsistencias = view.findViewById(R.id.rvAsistencias);
        tvCursoFrag = view.findViewById(R.id.tvCursoFrag);
        tvAsistioFrag = view.findViewById(R.id.tvAsistioFrag);


        //Inicializar variables para adaptador
        adapter = new AsistenciaAdapter(getContext());
        rvAsistencias.setAdapter(adapter);
        rvAsistencias.setLayoutManager(new GridLayoutManager(getContext(),1));

        //Asignar el id del alumno desde la cookie
        SharedPreferences preferences = this.getActivity().getSharedPreferences("appADOC",MODE_PRIVATE);
        idAlumno =  preferences.getInt("idAlumno",0);

        //Obtener fecha actual para cuando apenas ingresa
        etFechaAsistencia.setText(getFechaActual());
        fecha = getFechaActual();

        //Llamar a servicio para cuando apenas ingresa
        llamadaServicioAsis(fecha);

        //Acción al hacer clic en boton
        btnEditarFechaAsis.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mostrarCalendario();
                        }
                    });

        return view;
    }

    public void mostrarCalendario(){
        final Calendar cal = Calendar.getInstance();
        mDate = cal.get(Calendar.DATE);
        mMonth = cal.get(Calendar.MONTH);
        mYear = cal.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), android.R.style.Theme_DeviceDefault_Dialog
                , new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int date) {
                etFechaAsistencia.setText(date+"-"+(month+1)+"-"+year);
                fecha = year + "-" + (month+1) + "-" + date;
                llamadaServicioAsis(fecha);
            }
        },mYear,mMonth,mDate);
        datePickerDialog.show();
    }

    public void obtenerAsistenciasWS(String urlAsistencias){
        Log.i("urlTareas",urlAsistencias);
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        listaAsistencia = new ArrayList<Asistencia>();
        JsonArrayRequest jsonArray = new JsonArrayRequest(
                Request.Method.GET,
                urlAsistencias,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {
                        try{
                            if(jsonArray.length() > 0){
                                for(int i = 0; i < jsonArray.length(); i++){
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    listaAsistencia.add(new Asistencia(
                                            jsonObject.getString("nombre"),
                                            jsonObject.getString("asistencias")
                                    ));
                                }
                                adapter.agregarAsistencia(listaAsistencia);
                                tvCursoFrag.setVisibility(View.VISIBLE);
                                tvAsistioFrag.setVisibility(View.VISIBLE);
                            }
                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String json = null;
                        NetworkResponse response = error.networkResponse;
                        Log.i("response",Integer.toString(response.statusCode));
                        if(response != null && response.data != null){
                            switch (response.statusCode){
                                case 404:
                                    listaAsistencia.clear();
                                    adapter.agregarAsistencia(listaAsistencia);
                                    json = new String(response.data);
                                    json = extraerMensaje(json, "mensaje");
                                    if(json != null)
                                        mostrarMensaje(json);
                                    break;
                            }
                        }
                    }
                }
        );
        requestQueue.add(jsonArray);
    }

    public void llamadaServicioAsis(String fecha){
        urlAsistencias = "";
        urlAsistencias = "http://192.168.0.16:8090/awc/asistencia/listaAsisAlumnoFecha/" + idAlumno;
        urlAsistencias += "/" + fecha;
        obtenerAsistenciasWS(urlAsistencias);
    }

    public String extraerMensaje(String json, String key){
        String mensaje = null;
        try{
            JSONObject obj = new JSONObject(json);
            mensaje = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }
        return mensaje;

    }

    public void mostrarMensaje(String toastString){
        Toast toast = Toast.makeText(getContext(), toastString, Toast.LENGTH_LONG);
        View view = toast.getView();
        view.getBackground().setColorFilter(Color.parseColor("#605C3C"), PorterDuff.Mode.SRC_IN);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(Color.WHITE);
        toast.show();
        tvCursoFrag.setVisibility(View.GONE);
        tvAsistioFrag.setVisibility(View.GONE);
    }

    //Metodos Utilitarios
    public static String getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
        return formateador.format(ahora);
    }
}